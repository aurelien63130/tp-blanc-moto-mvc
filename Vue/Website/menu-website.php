<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php?controller=website&action=all">Toutes les motos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?controller=website&action=by-type&type=Sportive">Les sportives</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?controller=website&action=by-type&type=Custom">Les customs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?controller=website&action=by-type&type=Roadster">Les roadsters</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?controller=website&action=by-type&type=Enduro">Les enduros</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.php?controller=security&action=login">Administration</a>
            </li>

        </ul>
    </div>
</nav>