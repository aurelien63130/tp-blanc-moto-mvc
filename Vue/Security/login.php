<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php
    require 'Vue/Parts/global-stylesheets.php';
    ?>
</head>
<body>
<div class="container">
    <h1>Interface de connexion</h1>

    <form method="post">

    <div class="form-group">
        <label class="form-label" for="username">Username</label>
        <input type="text" name="username" class="form-control" id="username" placeholder="Nom d'utilisateur">
    </div>

    <div class="form-group">
        <label class="form-label" for="password">Password</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="Mot de passe">
    </div>

    <input type="submit" class="mt-2 btn btn-success">

        <?php
        require 'Vue/Parts/form-error.php'
        ?>
    </form>
</div>


<?php
require 'Vue/Parts/global-scripts.php';
?>

</body>
</html>