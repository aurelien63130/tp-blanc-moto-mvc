<?php
require "autoload.php";

if(empty($_GET)){
    header("Location: index.php?controller=website&action=all");
}
if($_GET["controller"] == 'moto'){
    $controller = new MotoController();

    if($_GET["action"] == 'list'){
        $controller->listMoto();
    }

    if($_GET["action"] == 'add'){
        $controller->addMoto();
    }

    if($_GET["action"] == 'edit' && isset($_GET["id"])){
        $controller->editMoto($_GET["id"]);
    }

    if($_GET["action"] == 'remove' && isset($_GET["id"])){
        $controller->removeMoto($_GET["id"]);

    }
}

if($_GET["controller"] == "security"){
    $controller = new SecurityController();
    if($_GET["action"] == 'login'){
        $controller->loginForm();
    }

    if($_GET["action"] == 'logout'){
        $controller->logout();
    }
}

if($_GET["controller"] == 'website'){
    $controller = new WebsiteController();
    if($_GET["action"] == 'all'){
        $controller->displayAll();
    }

    if($_GET["action"] == 'detail' && isset($_GET["id"])){
        $controller->detail($_GET["id"]);
    }

    if($_GET["action"] == 'by-type' && isset($_GET["type"])){
        $controller->byType($_GET["type"]);
    }

}
