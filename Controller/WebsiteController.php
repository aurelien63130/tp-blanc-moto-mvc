<?php
    class WebsiteController{
        private $motoManager;

        public function __construct(){
            $this->motoManager = new MotoManager();
        }

        public function displayAll(){
            $motos = $this->motoManager->findAll();

            require 'Vue/Website/list-moto.php';

        }

        public function displayByType($type){

        }

        public function detail($id)
        {
            $moto = $this->motoManager->find($id);
            require 'Vue/Website/detail-moto.php';
        }

        public function byType($type)
        {
            $motos = $this->motoManager->findByType($type);


            require 'Vue/Website/list-moto.php';
        }
    }
?>