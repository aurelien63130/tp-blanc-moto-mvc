<?php
class SecurityController {
    private $utilisateurManager;

    public function __construct(){
        $this->utilisateurManager = new UtilisateurManager();
    }

    public function loginForm(){
        $errors = [];

        if($_SERVER["REQUEST_METHOD"] == 'POST'){
            if(empty($_POST["username"])){
                $errors[] = 'Veuillez saisir un username';
            }
            if(empty($_POST["password"])){
                $errors[] = 'Veuillez saisir un mot de passe';
            }

            if(count($errors) == 0){
               $user = $this->utilisateurManager->findByUsername($_POST["username"]);

               if(!$user){
                   $errors[] = "Utilisateur inconnu";
               } else {
                   if(!password_verify($_POST["password"], $user->getPassword())){
                       $errors[] = "Mot de passe incorrect";
                   } else{
                        $session = new SessionService();
                        $session->user = serialize($user);

                        header("Location: index.php?controller=moto&action=list");
                   }
               }
            }
        }

        require 'Vue/Security/login.php';
    }

    public function logout()
    {
        session_destroy();
        header('Location: index.php?controller=security&action=login');
    }
}