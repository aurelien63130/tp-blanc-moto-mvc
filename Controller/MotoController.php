<?php

class MotoController extends AdminController
{
    private $motoManager;
    private $marqueManager;

    public function __construct()
    {
        parent::__construct();
        $this->motoManager = new MotoManager();
        $this->marqueManager = new MarqueManager();
    }

    public function listMoto()
    {
        // Que j'aille chercher toutes les motos dans la db
        $motos = $this->motoManager->findAll();

        require 'Vue/Moto/listing.php';
    }

    public function addMoto()
    {

        $errors = [];
        $marques = $this->marqueManager->findAll();

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Vérifier le formulaire
            $errors = $this->checkForm($errors, $marques);
            $uniqFilename = null;


            if (count($errors) == 0 && $_FILES["picture"]["error"] != 4) {
               $upload =  $this->uploadImage($errors);
               $uniqFilename = $upload["filename"];
               $errors = $upload["errors"];
            }


            // Enregistrer dans la BDD
            if(count($errors) == 0){
                $moto = new Moto(null, $_POST["marque"], $_POST["modele"], $_POST["type"], $uniqFilename);

                $this->motoManager->add($moto);

                header("Location: index.php?controller=moto&action=list");
            }

            // Rediriger l'utilisateur
        }

        // J'irais afficher mon formulaire !
        require 'Vue/Moto/form.php';
    }

    private function checkForm($errors, $marques)
    {


        if (empty($_POST["marque"])) {
            $errors[] = "Veuillez saisir une marque";
        }

        $marqueOk = false;
        foreach ($marques as $marque) {

            if ($marque->getId() == $_POST["marque"]) {
                $marqueOk = true;
            }
        }
        if (!$marqueOk && !empty($_POST["marque"])) {
            $errors[] = 'La marque n\'est pas connue de nos services';
        }

        if (empty($_POST["modele"])) {
            $errors[] = "Veuillez saisir un modele";
        }

        if (empty($_POST["type"])) {
            $errors[] = "Veuillez saisir un type";
        }

        if (!in_array($_POST["type"], ["Enduro", "Custom", "Sportive", "Roadster"]) && !empty($_POST["type"])) {
            $errors[] = "Ce type n'est pas possible";
        }

        return $errors;
    }

    public function editMoto($id)
    {
        $errors = [];
        $marques = $this->marqueManager->findAll();
        $editMoto = $this->motoManager->find($id);

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Validation de notre formulaire
            $uniqFilename = null;
            $errors = $this->checkForm($errors, $marques);
            // Doit on uploader une image;
            if(count($errors) == 0 && $_FILES["picture"]["error"] != 4) {
                $upload =  $this->uploadImage($errors);
                $uniqFilename = $upload["filename"];
                $errors = $upload["errors"];
            } else {
                $uniqFilename = $editMoto->getImage();
            }

            if(count($errors) == 0){
                $editMoto->setImage($uniqFilename);
                $editMoto->setMarque($_POST["marque"]);
                $editMoto->setModel($_POST["modele"]);
                $editMoto->setType($_POST["type"]);

                $this->motoManager->update($editMoto);

                header("Location: index.php?controller=moto&action=list");
            }
        }
        require 'Vue/Moto/form.php';


    }

    private function uploadImage($errors){
        // Uploader l'image
        if ($_FILES["picture"]["error"] != 0) {
            $errors[] = 'Une erreur dans l\'upload';
        }
        $types = ["image/jpeg", "image/png"];
        if (!in_array($_FILES["picture"]["type"], $types)) {
            $errors[] = 'Nous n\'acceptons que les images jpeg / png';
        }

        if (!in_array($_FILES["picture"]["type"], $types)) {
            $errors[] = 'Nous n\'acceptons que les images jpeg / png';
        }

        if ($_FILES["picture"]["size"] > 5 * 1048576) {
            $errors[] = 'Le fichier ne doit pas dépasser 5 Mo';
        }

        if(count($errors) == 0){
            $extension = explode("/", $_FILES["picture"]["type"])[1];
            $uniqFilename = uniqid().'.'.$extension;
            move_uploaded_file($_FILES["picture"]["tmp_name"], 'Public/uploads/'.$uniqFilename);
        }

        return ["errors"=> $errors, 'filename'=> $uniqFilename];
    }

    public function removeMoto($id)
    {
        // On supprime la moto
        $this->motoManager->remove($id);

        // On redirige l'utilisateur
        header("Location: index.php?controller=moto&action=list");
    }
}

?>