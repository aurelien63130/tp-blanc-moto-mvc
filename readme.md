**Methodes magiques** 

- __construct() Il est déclanché quand on instancie un nouvel objet avec le mot clé num

`````php
<?php
class MaClasse {
    public function __construct(){
        
    }
}

// Le mot clé new déclanche le constructeur
$obj = new Maclasse();
`````

- __destruct() Il est déclanché quand on supprime notre objet. Il est appelé implicitement à la fin d'un script
`````php
<?php
class MaClasse {
    public function __destruct(){
    
    }
}

$obj = new MaClasse();

// Mon destructeur est déclanché en fin de script

// Ou en faisant 
unset($obj);

`````


- __get($name) Cette méthode est appelée quand on essaie d'accéder à un attribut inaccessible (privé, protégé, inexistant)

````php
<?php
    class MaClasse{
        public function __get($name){
            // Ici name est égal à l'attribut auxquel on tente d'accéder
        }
    }
    
    $obj = new MaClasse();
    // On passe dans notre méthode magique, $name = "test"
    echo($obj->test);
?>

````

- __set($name, $value) Cette méthode est appelée quand on essaie d'assigner une valeur à un attribut inaccessible (privé, protégé, inexistant)


````php
<?php
    class MaClasse{
        public function __set($name, $value){
            // Ici name est égal à l'attribut auxquel on tente d'accéder
            // $value sera égale à la valeur que l'on a voulu assigner
        }
    }
    
    $obj = new MaClasse();
    // On passe dans notre méthode magique, $name = "firstname" et $value = "Aurélien"
    $obj->firstname = "Aurélien"
?>

````

- __isset($name) Cette méthode est appelée quand on appel la fonction php isset sur un attribut inaccessible (privé, protégé, inexistant)


````php
<?php
    class MaClasse{
        public function __set($name){
            // Ici name est égal à l'attribut auxquel sur lequel on applique la fonction php isset
        }
    }
    
    $obj = new MaClasse();
    // On passe dans notre méthode magique, $name = "firstname"
    isset($obj->firstname);
?>

````