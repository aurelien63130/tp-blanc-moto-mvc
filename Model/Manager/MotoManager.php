<?php
    class MotoManager extends DbManager {
        public function findAll(){
            $query = $this->bdd->prepare('SELECT * FROM moto JOIN marque
             ON moto.id_marque = marque.id');
            $query->execute();
            $resultats = $query->fetchAll();
            $arrayObject = [];

            foreach ($resultats as $resultat){
                $marque = new Marque($resultat["id_marque"], $resultat[6]);


                $moto = new Moto($resultat[0], $marque, $resultat["modele"], $resultat["type"], $resultat["lien_image"]);

                $arrayObject[] = $moto;
            }

            return $arrayObject;
        }

        public function add(Moto $moto)
        {
            $query = $this->bdd->prepare("INSERT INTO moto (id_marque, modele, type, lien_image) VALUES 
            (:idMarque, :modele, :type, :lienImage)");

            $query->execute([
                "idMarque"=> $moto->getMarque(),
                'modele'=> $moto->getModel(),
                "type"=> $moto->getType(),
                "lienImage"=> $moto->getImage()
            ]);
        }

        public function find($id)
        {
            $query = $this->bdd->prepare('SELECT * FROM moto JOIN marque
             ON moto.id_marque = marque.id WHERE moto.id = :id');

            $query->execute([
                "id"=> $id
            ]);

            $resultat = $query->fetch();

            $marque = new Marque($resultat["id_marque"], $resultat[6]);
            $moto = new Moto($resultat[0], $marque, $resultat["modele"], $resultat["type"], $resultat["lien_image"]);

            return $moto;
        }

        public function update(Moto $editMoto)
        {
            $query = $this->bdd->prepare("UPDATE moto
            SET id_marque = :marque, modele = :modele, type=:type, lien_image = :image WHERE id = :id");

            $query->execute([
                "marque"=> $editMoto->getMarque(),
                'modele'=> $editMoto->getModel(),
                'type'=> $editMoto->getType(),
                "image"=> $editMoto->getImage(),
                "id"=> $editMoto->getId()
            ]);
        }

        public function remove($id)
        {
            $query = $this->bdd->prepare("DELETE FROM moto WHERE id = :id");
            $query->execute([
                "id"=> $id

                ]);
        }

        public function findByType($type)
        {
            $arrayObject = [];
            $query = $this->bdd->prepare('SELECT * FROM moto JOIN marque
             ON moto.id_marque = marque.id WHERE moto.type = :type ');

            $query->execute([
                "type"=> $type
            ]);

            $resultats = $query->fetchAll();

            foreach ($resultats as $resultat){
                $marque = new Marque($resultat["id_marque"], $resultat[6]);


                $moto = new Moto($resultat[0], $marque, $resultat["modele"], $resultat["type"], $resultat["lien_image"]);

                $arrayObject[] = $moto;
            }

            return $arrayObject;
        }
    }
?>