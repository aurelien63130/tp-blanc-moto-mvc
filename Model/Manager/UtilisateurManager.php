<?php
class UtilisateurManager extends DbManager {

    public function findByUsername($username){
        $user = null;

        $query = $this->bdd->prepare("SELECT * FROM utilisateur WHERE username = :username");
        $query->execute([
            "username"=> $username
        ]);

        $result = $query->fetch();

        if($result){
            $user = new Utilisateur($result["id"], $result["username"], $result["password"]);
        }

        return $user;
    }
}