<?php
abstract class DbManager {
    protected $bdd;

    public function __construct(){
        $this->bdd = new PDO("mysql:dbname=exam_poo;host=database","root","tiger");
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}
?>